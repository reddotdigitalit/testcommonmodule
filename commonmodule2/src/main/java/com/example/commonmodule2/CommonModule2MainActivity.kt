package com.example.commonmodule2

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView

class CommonModule2MainActivity : AppCompatActivity(), View.OnClickListener {


    lateinit var editText: EditText
    lateinit var textView: TextView
    lateinit var  Button: Button


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_common_module2_main)

        editText = findViewById(R.id.edittext)
        textView = findViewById(R.id.textview)
        Button = findViewById(R.id.displaytext)


        Button.setOnClickListener(this)

    }

    override fun onClick(v: View?)
    {
        val name = editText.text
        textView.text = name
    }
}