package com.example.testcommonmodule

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.TextView


//class MainActivity : AppCompatActivity() {
//    override fun onCreate(savedInstanceState: Bundle?) {
//        super.onCreate(savedInstanceState)
//        setContentView(R.layout.activity_main)
//    }
//}

class MainActivity : AppCompatActivity(), View.OnClickListener  {
    lateinit var editText: EditText
    lateinit var textView: TextView
    lateinit var  Button: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_common_module2_main)

        editText = findViewById(com.example.commonmodule2.R.id.edittext)
        textView = findViewById(com.example.commonmodule2.R.id.textview)
        Button = findViewById(com.example.commonmodule2.R.id.displaytext)


        Button.setOnClickListener(this)


        Button.setOnClickListener(this)

    }

    override fun onClick(p0: View?) {
        val name = editText.text
        textView.text = name
        Log.e("Test", "Clicked" )
    }
}